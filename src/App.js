import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Table from "./components/Table/Table";
import Form from "./components/Form/Form";

export const App = () => {
    return (
        <Router>
            <Switch>
                <Route exact path="/">
                    <div className="App">
                        <Link to="table">Таблица</Link>
                        <Link to="form">Форма</Link>
                    </div>
                </Route>
                <Route exact path="/table">
                    <Table />
                </Route>
                <Route exact path="/form">
                    <Form />
                </Route>
            </Switch>
        </Router>
    );
};
