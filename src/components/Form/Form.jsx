import s from "./Form.module.css";
import { useCallback, useEffect, useState } from "react";
import { useInput } from "../../hooks";
import { Hide, Show } from "../SpriteSVG/SpriteSVG";
import zxcvbn from 'zxcvbn'
import PasswordStrengthIndicator from "../PasswordStrengthIndicator/PasswordStrengthIndicator";

export default function Form() {
    const [inputTypeIsPassword, setInputTypeIsPassword] = useState(true);

    const [currentPasswordStrength, setCurrentPasswordStrength] = useState(0)

    const [validateEveryChange, setValidateEveryChange] = useState(false);

    const [formIsValid, setFormIsValid] = useState({
        login: true,
        password: true,
    });

    const [formState, setFormState] = useState({
        login: "",
        password: "",
    });

    const showHidePassword = () => {
        setInputTypeIsPassword((prev) => !prev);
    };

    const validateForm = useCallback(() => {
        const newFormState = {
            login: formState.login.length >= 2,
            password: formState.password.length >= 6,
        };
        setFormIsValid(newFormState);
        const hasFalse = Object.values(newFormState).includes(false);
        if (hasFalse) {
            setValidateEveryChange(true);
        }
        return !hasFalse;
    }, [formState.login.length, formState.password.length]);

    const sendData = (e) => {
        e.preventDefault();
        if (validateForm()) {
            alert("Send form");
        }
    };

    useEffect(() => {
        if (validateEveryChange) validateForm();
        setCurrentPasswordStrength(zxcvbn(formState.password).score)
    }, [formState, validateForm, validateEveryChange]);

    return (
        <div className={s.wrapper}>
            <form className={s.auth} onSubmit={sendData}>
                <label className={s.inputItem}>
                    <input
                        autoFocus={true}
                        {...useInput(
                            formState,
                            setFormState,
                            "login",
                            "onInput"
                        )}
                        placeholder="Логин"
                        type="text"
                    />
                    {!formIsValid.login && (
                        <span className={s.inputError}>
                            Длина имени - не менее 2 символов!
                        </span>
                    )}
                </label>
                <label className={`${s.inputItemIcon} ${s.inputItem}`}>
                    <span className={s.eye} onClick={showHidePassword}>
                        {inputTypeIsPassword ? <Show /> : <Hide />}
                    </span>
                    <input
                        {...useInput(
                            formState,
                            setFormState,
                            "password",
                            "onInput"
                        )}
                        type={inputTypeIsPassword ? "password" : "text"}
                        placeholder="Пароль"
                    />
                    <PasswordStrengthIndicator amount={currentPasswordStrength}/>
                    {!formIsValid.password && (
                        <span className={s.inputError}>
                            Длина пароля - не менее 6 символов!
                        </span>
                    )}
                </label>
                <button className={s.button}>Войти</button>
            </form>
        </div>
    );
}
