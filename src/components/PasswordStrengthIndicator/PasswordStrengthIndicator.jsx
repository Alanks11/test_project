import { useEffect, useState } from 'react'
import s from './PasswordStrengthIndicator.module.css'

export default function PasswordStrengthIndicator({amount}){

const [currentColor, setCurrentColor] = useState('')

const [currentWidth, setCurrentWidth] = useState(0)

const colors = {
    '0': '',
    '1': 'red',
    '2': 'orange',
    '3': 'yellow',
    '4': 'green',
}

const widths = {
    '0': 0,
    '1': 25,
    '2': 50,
    '3': 75,
    '4': 100,
}

useEffect(()=>{
    setCurrentColor(colors[amount])
    setCurrentWidth(widths[amount])
}, [amount])

    return(
        <div className={s.wrapper}>
            <span className={s.level} style={{
                backgroundColor: currentColor,
                width: currentWidth + '%'
            }}/>
        </div>
    )
}
