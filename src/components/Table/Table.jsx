import { useState } from "react";
import s from "./Table.module.css";
import cn from "classnames";

export default function Table() {
    const [activeCells, setActiveCells] = useState([]);

    const handler = (e) => {
        const id = e.target.dataset.id;
        activeCells.includes(id)
            ? setActiveCells((prev) => prev.filter((item) => item !== id))
            : setActiveCells((prev) => [...prev, id]);
    };

    const tableSize = 100;

    return (
        <div className={s.wrapper}>
            <table className={s.table} onClick={handler}>
                <tbody>
                    {new Array(tableSize).fill(0).map((item, a) => {
                        return (
                            <tr key={`${a}tr`}>
                                {new Array(tableSize).fill(0).map((item, b) => {
                                    return (
                                        <td
                                            key={a * 100 + b}
                                            data-id={a * 100 + b}
                                            className={cn({
                                                [s.active]:
                                                    activeCells.includes(
                                                        (a * 100 + b).toString()
                                                    ),
                                            })}
                                        />
                                    );
                                })}
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}
