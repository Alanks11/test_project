export function useInput(state, setState, name, on) {
    const handler = (e) => {
        setState((prev) => {
            return {
                ...prev,
                [name]: e.target.value,
            };
        });
    };
    return {
        name: name,
        [on]: handler,
        value: state[name],
    };
}
